import {Container, Row, Col, Button, Card} from 'react-bootstrap';

import {useContext} from 'react';

import {Link} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function ProductCard(props){
	const {user} = useContext(UserContext);

	const {_id, name, description, price} = props.productProp;
	
	return(
		<Container className = 'mt-3'>
			<Row>
				<Col>
					<Card>
				      <Card.Body>
				        <Card.Title className='text-center mb-3'>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				        	{description}
				        </Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				        	{price}
				        </Card.Text>
				      </Card.Body>
				      {
						localStorage.getItem('token') !== null
						?
						<Button as = {Link} to = {`/products/${_id}`} className = 'col-3 col-xs-2 col-sm-2 col-md-2 col-lg-1 ms-2 mb-2'>Details</Button>
						:
						<Button as = {Link} to = '/login' className = 'col-3 col-xs-2 col-sm-2 col-md-2 col-lg-1 ms-2 mb-2'>Login to Shop</Button>
					  }
				    </Card>
				</Col>
			</Row>
		</Container>
		);
}