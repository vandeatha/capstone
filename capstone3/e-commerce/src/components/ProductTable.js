import {useContext} from 'react';

import {Button} from 'react-bootstrap';

import {Link, useParams} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function ProducTable({onDisable, onActivate, ...props}){
	const {user} = useContext(UserContext);

	const {_id, name, description, price, isActive} = props.productProp;

    const {productId} = useParams();

	return(
        <tbody>
            <tr>
                <td>{name}</td> 
                <td>{description}</td>
                <td>{price}</td>
                <td>
                    {
                        isActive === true
                        ?
                        'Available'
                        :
                        'Unavailable'
                    }
                </td>
                <td>
                    <Button variant="primary" type="submit" className='mb-1 col-12' as = {Link} to = {`products/${_id}`}>Update</Button>
                    {
                        isActive === true
                        ?
                        <Button variant="danger" type="submit" className='mb-1 col-12' onClick = {onDisable}>Disable</Button>
                        :
                        <Button variant="success" type="submit" className='mb-1 col-12' onClick = {onActivate}>Activate</Button>
                    }
                </td>
            </tr>
        </tbody>
    );
}