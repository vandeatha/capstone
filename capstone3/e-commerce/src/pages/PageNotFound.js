import {Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import PageNotFoundImage from '../images/404error.svg';

export default function (){
    return(
        <Container className = 'mt-3 text-center'>
            <img src = {PageNotFoundImage} alt = '404 error image' style = {{height: '400px'}}/>
            <p>Go back to the <Link to = '/' className = 'text-decoration-none'>homepage</Link></p>
        </Container>
    );
}   