import ProductCard from '../components/ProductCard.js';

import {useState, useEffect} from 'react';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return (
					<ProductCard key = {product._id} productProp = {product}/>
				);
			}))
		})
	}, [])


	return(
		<>
			<h1 className='text-center mt-3'>Products</h1>
			{products}
		</>
		)
}