const Products = require('../models/productsModel.js');
const auth = require('../auth.js');

// Creating a product
module.exports.createProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		});

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false);
	};
	
};

// Retrieving all products
module.exports.getAllProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Products.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
	else{
		return response.send(error);
	};
};

// Retrieving specific product
module.exports.getProduct = (request, response) => {
	const productId = request.params._id;

	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
};

// Updating specific product (using params)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false)
	}
};

// Archiving specific product
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let archivedProduct = {
		isActive: false
	};

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		 return response.send(false);
	};
};

// Retrieving active products
module.exports.getActiveProducts = (request, response) => {
	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
};

// Activating specific product
module.exports.activateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let activatedProduct = {
		isActive: true
	};

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, activatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false);
	};
};