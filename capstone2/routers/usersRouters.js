const express = require('express');
const usersControllers = require('../controllers/usersControllers.js');
const auth = require('../auth.js');

const router = express.Router();

// Admin registration
router.post('/admin-signup', usersControllers.adminRegistration);
// User registration
router.post('/signup', usersControllers.userRegistration);
// Login
router.post('/login', usersControllers.login);
// Retrieve user details
router.get('/retrieveUserDetails', auth.verify, usersControllers.retrieveUserDetails);
// Setting user as admin
router.patch('/:_id', auth.verify, usersControllers.updateUserType);


module.exports = router;