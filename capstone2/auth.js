const jwt = require('jsonwebtoken');

const secret = 'EcommerceAPI';

module.exports.createAccessToken = (user) => {
	const userData = {
		id: user._id,
		isAdmin: user.isAdmin
	}
	// console.log(userData);
	return jwt.sign(userData, secret, {});
};

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send(false);
			}
			else{
				next();
			};
		});
	}
	else{
		return response.send(false);
	};
};

module.exports.decode = (token) => {
	token = token.slice(7, token.length);
	return jwt.decode(token, {complete: true}).payload;
};