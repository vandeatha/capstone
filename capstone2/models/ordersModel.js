const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	products: [{
		productId: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Product',
			required: true
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

const Orders = mongoose.model('Order', orderSchema);

module.exports = Orders;