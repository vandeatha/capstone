const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: Object,
		required: true
	},
	products: [{
		productId: {
			type: Object,
			required: true
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	subTotal: {
		type: Number,
		required: true
	},
	totalAmount: {
		type: Number,
		required: true
	}
});

const Carts = mongoose.model('Cart', cartSchema);

module.exports = Carts;